<?php
    // Constants
    const DBNAME    = "developereval";  // db name
    const TABLENAME = "users";  // db table name
    const SHORTOPTS ="u:p:h:";  // command line parameters
    const LONGOPTS = [
        "file:",
        "create_table",
        "dry_run",
        "help"
    ];
    const MESSAGES = [
        "table_created" => "Table successfully (re)created".PHP_EOL,
        "read" => "%d user(s) read from file".PHP_EOL,
        "processed" => "%d user(s) processed".PHP_EOL,
        "invalid_email" => "Invalid email %s, skipping".PHP_EOL,
        "csv_parse_error" => "Cannot parse line %d of input file".PHP_EOL,
        "file_do_not_exist" => "Cannot find file %s".PHP_EOL,
        "file_read_error" => "Cannot read file %s".PHP_EOL,
        "no_options" => "Either --file or --create_table shoud be set".PHP_EOL,
        "aborted" => "Execution aborted".PHP_EOL,
        "db_connect_error" => "MySQL connection error: %s".PHP_EOL,
        "db_query_error" => "MySQL error: %s".PHP_EOL,
        "db_insert_error" => "Error while inserting record: %s".PHP_EOL,
        "dry_run" => "Processing in dry run mode".PHP_EOL,
    ];
    const HELPDOC = <<<EOT
Upload user list to MySQL database
Usage: user_upload.php [options]
    -u <uname>       MySQL username
    -p <passwd>      MySQL password
    -h <host>        MySQL host
    --file <file>    input file in CSV format
    --create_table   create/reset database table (DATA WILL BE LOST)
    --dry_run        test run, data will not be stored in database
    --help           this help
    
EOT;

    // parsing command line arguments
    $options = getopt(SHORTOPTS, LONGOPTS);

    // getting parameters
    $dbUser = isset($options["u"]) ? $options["u"] : "";
    $dbPassword = isset($options["p"]) ? $options["p"] : "";
    $dbHost = isset($options["h"]) ? $options["h"] : "";
    $fileName = isset($options["file"]) ? $options["file"] : "";

    $optionHelp = isset($options["help"]);
    $optionCreateTable = isset($options["create_table"]);
    $optionDryRun = isset($options["dry_run"]);

    // Start of main processing block
    try {

        if ($optionHelp || empty($options) ) {
            // --help processing (also if no known parameters were given)
            echo HELPDOC;

        } elseif ($optionCreateTable) {
            //--create_table processing
            $db = dbConnect($dbHost, $dbUser, $dbPassword);
            createTable($db);
            echo MESSAGES["table_created"];

        } else {
        // otherwise - process file
        // (pass dry_run value to function that will commit changes to db)
            $users = readUsersFromCSV($fileName);
            printf(MESSAGES["read"], count($users));
            $users = validateAndFormatUsers($users);
            $db = dbConnect($dbHost, $dbUser, $dbPassword);
            $processedCount = importUsers($db, $users, $optionDryRun);
            printf(MESSAGES["processed"], $processedCount);
        }

    } catch (Exception $e) {
        echo $e->getMessage();
        echo MESSAGES["aborted"];
    }
    finally {
        if (isset($db)) {
            dbDisconnect($db);
        }
    }
    // END OF MAIN PROCESSING BLOCK


    function dbConnect($dbHost, $dbUser, $dbPassword) {
        /* connect to database
         * $dbHost:     host to connect
         * #dbUser:     mySQL user
         * $dbPassword: mySQL user's password
         * returns DB object
         * throws Exception
         */

        $db = new mysqli($dbHost, $dbUser, $dbPassword);

        if ($db->connect_error) {
            throw new Exception(sprintf(MESSAGES["db_connect_error"],
                $db->connect_error));
        }

        return $db;
    }

    function dbDisconnect($db) {
        /* disconnect from database
         * $db: DB object
         * returns none
         */

         $db->close();
    }

    function createTable($db) {
        /* creates table in database
         * $db: DB object
         * returns none
         * throws Exception
         */

        // create db if not exists
        $db->query("CREATE DATABASE IF NOT EXISTS ".DBNAME);

        if ($db->error) {
            throw new Exception(sprintf(MESSAGES["db_query_error"],
                $db->error));
        }

        $db->select_db(DBNAME);

        if ($db->error) {
            throw new Exception(sprintf(MESSAGES["db_query_error"],
                $db->error));
        }

        // delete table if table exists
        $db->query("DROP TABLE IF EXISTS ".TABLENAME);

        if ($db->error) {
            throw new Exception(sprintf(MESSAGES["db_query_error"],
                $db->error));
        }

        // create table
        $db->query("CREATE TABLE ".TABLENAME." (
            userid    INT NOT NULL AUTO_INCREMENT,
            firstname VARCHAR(255),
            surname   VARCHAR(255),
            email     VARCHAR(255),
            PRIMARY KEY(userid),
            UNIQUE INDEX(email)
        )");

        if ($db->error) {
            throw new Exception(sprintf(MESSAGES["db_query_error"],
                $db->error));
        }
    }

    function readUsersFromCSV($fileName) {
        /* reads user list from CSV file
         * $fileName: name of CSV file
         * returns array of arrays
         * trhows Exception
         */

        $users = array();

        if ($fileName == "") {
            throw new Exception(MESSAGES["no_options"]);
        }

        if (!file_exists($fileName)) {
            throw new Exception(sprintf(MESSAGES["file_do_not_exist"], $fileName));
        }

        $lineNumber = 1;
        if (($fp = fopen($fileName, "r")) !== false) {
            // read CSV line by line
            while (($csvData = fgetcsv($fp)) !== false) {
                if (count($csvData) == 3) {
                    $users[] = ["firstname" => $csvData[0],
                               "surname" => $csvData[1],
                               "email" => $csvData[2]];
                } elseif (!empty(array_filter($csvData))) {
                    // non-empty line that haven't 3 parts
                    printf(MESSAGES["csv_parse_error"], $lineNumber);
                }
                $lineNumber++;
            }
            fclose($fp);
        } else {
            throw new Exception(sprintf(MESSAGES["file_read_error"], $fileName));
        }
        array_shift($users);  // removing header of CSV file
        return $users;
    }

    function validateAndFormatUsers($users) {
        /* validates user emails and formats user names
         * $users: array of arrays
         * returns array of arrays
         */

        // change register
        foreach ($users as &$user) {
            $user["firstname"] = ucwords(strtolower(trim($user["firstname"])), " -'");
            $user["surname"] = ucwords(strtolower(trim($user["surname"])), " -'");
            $user["email"] = strtolower(trim($user["email"]));
        }
        unset($user);

        // filter invalid emails
        $users = array_filter($users, function ($user) {
            if (filter_var($user["email"], FILTER_VALIDATE_EMAIL)) {
                return true;
            } else {
                printf(MESSAGES["invalid_email"], $user["email"]);
            };
        } );

        return $users;
    }

    function importUsers($db, $users, $dryRun) {
        /* writes users to DB table
         * $db:     DB object
         * $users:  array of arrays
         * $dryRun: commit data or not (boolean)
         * returns number of imported records
         * throws Exception
         */

        $counter = 0;

        if ($dryRun) {
            echo MESSAGES["dry_run"];
        }

        $db->select_db(DBNAME);

        if ($db->error) {
            throw new Exception(sprintf(MESSAGES["db_query_error"],
                $db->error));
        }

        $db->begin_transaction();

        foreach($users as $user) {
            $values = "('".mysqli_real_escape_string($db, $user["firstname"]).
                    "', '".mysqli_real_escape_string($db, $user["surname"]).
                    "', '".mysqli_real_escape_string($db, $user["email"])."')";

            if ($db->query(
                "INSERT INTO ".TABLENAME." (firstname, surname, email) VALUES $values")) {
                $counter++;
            } else {
                printf(MESSAGES["db_insert_error"], $db->error);
            }
        }

        if ($dryRun) {
            $db->rollback();
        } else {
            $db->commit();
        }

        return $counter;
    }
?>
