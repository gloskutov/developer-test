<?php
    const COUNT = 100;  // number of elements to print

    $output = [];

    for ($i = 1; $i <= COUNT; $i++) {
        $output[$i] = "";
        if ($i % 3 == 0) {          // divisible by 3
            $output[$i] .= 'foo';
        }
        if ($i % 5 == 0) {          // divisible by 5
            $output[$i] .= 'bar';
        }
        if ($output[$i] == "") {    // not divisible by 3 or 5
            $output[$i] = $i;
        }
    }

    echo join(", ", $output).PHP_EOL;
?>
