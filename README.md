# Developer evaluation

## Script Task

PHP script is executed from the command line and accepts a CSV file as input
(see command line directives below) and processes the CSV file. The parsed file data is to be
inserted into a MySQL database.

### Assumptions

* I assume PHP version is >=5.6 (since 5.5 End Of Life was 10/06/16), so I used constant arrays that were introduced in 5.6. For earlier versions you should look at php-pre-56 branch
* There was no database name in the task. I assume that if database "developereval" exists, mysql user has manager rights for it (if this database name do not exist, it will be created).
* There are no default values to command line directives
* Emails should be checked against RFC 2822
* It's not clear from the design, should MySQL table be built when --file option is set, or should it be built only if --create_table is set. I assume the latter, table will be created only if --create_table is set, and script run with --file option will produce error message if table do not exist. So, if table do not exist, user should run script with --create_table first
* CSV file has header in the first line. Lines with 3 values will be processed, empty lines will be ignored, other lines will be reported to user and ignored


### Command Line Directives
```php
-u <uname>
```
MySQL username
```php
-p <passwd>
```
MySQL password
```php
-h <host>        
```
MySQL host
```php
--file <file>
```
Input file in CSV format
```php
--create_table
```
Create/reset database table (if table exists, it will be overwritten)
```php
--dry_run
```
Test run, data will not be stored in database
```php
--help
```
Help page

### Usage example
Create an empty table
```php
php user_upload.php -u username -p password -h hostname --create_table
```

Test run of user import
```php
php user_upload.php -u username -p password -h hostname --file users.csv --dry_run
```

User import
```php
php user_upload.php -u username -p password -h hostname --file users.csv
```

## Logic Test

The script:

* Outputs the numbers from 1 to 100
* Where the number is divisible by three (3) outputs the word “foo”
* Where the number is divisible by five (5) outputs the word “bar”
* Where the number is divisible by three (3) and five (5) outputs the word “foobar”

### Command Line Directives

None.

### Usage example

```php
php foobar.php
```